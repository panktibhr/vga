import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router, RouterLink } from "@angular/router";
import { User } from "../@core/models/user.model";
import { LoginService } from "../@core/services/login.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.css"],
})
export class SigninComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  returnUrl = "/dashboard";
  error = false;
  passwordHidden = true;
  last_login_on: User;
  // user_id: User;
  subscription;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ["", [Validators.required]],
      password: ["", [Validators.required]],
    });
    this.subscription = this.activatedRoute.snapshot.queryParams["user_id"];
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params["returnUrl"]) {
        this.returnUrl = params["returnUrl"];
      }
    });
  }
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  // get username() { return this.loginForm.get('username'); }
  // get password() { return this.loginForm.get('password'); }

  onSubmit() {
    this.error = false;
    this.submitted = true;

    // if (this.loginForm.invalid) {
    //   return;
    // }

    this.loginService
      .login(this.f.username.value, this.f.password.value)
      .subscribe(
        (data) => {
          if (data) {
            localStorage.setItem("token", data.token);
            localStorage.setItem("id", data.user.id);

            this.router.navigate(["/dashboard"]);
            this.loginService
              .lastLogin(data.user.id)
              .subscribe((data: any) => {
                console.log(data);
              });
          } else {
            this.error = true;
          }
        },
        (error) => {
          this.error = error;
        }
      );

    // this.loginService.lastLogin( this.user_id ).subscribe((data:any) => {
    //   if(data){
    //     // console.log(data);
    //     // console.log(localStorage.getItem('id'));
    //     // localStorage.getItem('id');
    //     // console.log(data.user.id);
    //     this.loginService.lastLogin(this.user_id);
    //   }
    // })
  }

  togglePassword() {
    this.passwordHidden = !this.passwordHidden;
  }
}
