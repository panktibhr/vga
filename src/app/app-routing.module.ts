import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './signin/signin.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { PagesModule } from './pages/pages.module';
import { LockScreenComponent } from './lock-screen/lock-screen.component';
import { HomeComponent } from './home/home.component';
import { PasswordResetFormComponent } from './password-reset-form/password-reset-form.component';
import { AuthGuardGuard } from './@core/auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: 'signin', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent },
  { path: 'change-password', component: PasswordResetFormComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'lock-screen', component: LockScreenComponent },
  { path: 'home', component: HomeComponent },
  { path: 'pages', canActivate: [AuthGuardGuard], loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule) },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, PagesModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
