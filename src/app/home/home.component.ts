import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../@core/services/login.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public loginService: LoginService, public router: Router) { }

  ngOnInit() {

  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/signin']);
  }

}
