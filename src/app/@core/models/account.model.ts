export interface Account {
  id?: string;
  name?: string;
  description?: string;
  email?: string;
  phone?: number;
  parent_account_id?: string;
}
