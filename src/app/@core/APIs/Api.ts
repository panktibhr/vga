import { environment } from "src/environments/environment";

const APIs = {
  login: `${environment.BASE_URL}/auth/login`,
  lastLogin: `${environment.BASE_URL}/users/update/last-login-date`,
  forgotPassword: `${environment.BASE_URL}/auth/forgot-password`,
  changePassword: `${environment.BASE_URL}/auth/update-password`,
};
