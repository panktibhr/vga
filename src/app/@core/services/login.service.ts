import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { map } from "rxjs/operators";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "../models/user.model";

@Injectable({
  providedIn: "root",
})
export class LoginService {
  public currentUserSubject: BehaviorSubject<User>;
  public currentUserToken: BehaviorSubject<string>;
  public currentUser: Observable<User>;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<User>(
      JSON.parse(JSON.stringify(localStorage.getItem("id")))
    );
    this.currentUserToken = new BehaviorSubject<string>(
      localStorage.getItem("token")
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // public get currentUserValue(): User {
  //   return this.currentUserSubject.value;
  // }

  public get token(): string {
    return this.currentUserToken.value;
  }

  login(username: string, password: string): Observable<any> {
    return this.httpClient.post<any>(`${environment.BASE_URL}/auth/login`, {
      username: username,
      password: password,
    });
  }

  lastLogin(user_id: any): Observable<any> {
    return this.httpClient.post<any>(
      `${environment.BASE_URL}/users/update/last-login-date`, { user_id }
    );
  }

  forgotPassword(email): Observable<any> {
    return this.httpClient.post<any>(
      `${environment.BASE_URL}/auth/forgot-password`,
      { email }
    );
  }

  changePassword(user_id: string, pass: string, confirm_pass: string) {
    return this.httpClient.post(
      `${environment.BASE_URL}/auth/update-password${user_id}`,
      { id: user_id, password: pass, confirmPassword: confirm_pass }
    );
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('id');
  }

  authService() {
    const user = localStorage.getItem('token');
    return !(user === null);
  }

  // lastLogin(user_id: User){
  //   return this.httpClient.post<any>(`${environment.BASE_URL}/users/update/last-login-date`, { user_id: user_id })
  //   .pipe(map(response => {
  //     console.log(response);
  //       // localStorage.setItem('id', response.id);
  //       // localStorage.setItem('last_login_on', response.last_login_on)
  //       // this.currentUserSubject.next(response.data);
  //       // this.currentUserToken.next(response.data.last_login_on);
  //     return response;
  //   }))
  // }
}
