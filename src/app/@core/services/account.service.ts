import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Router } from "@angular/router";
import { environment } from "src/environments/environment";
import { BehaviorSubject, Observable } from "rxjs";
import { Account } from '../models/account.model';

@Injectable({
  providedIn: "root",
})
export class AccountService {
  public currentUserSubject: BehaviorSubject<Account>;
  public currentUserToken: BehaviorSubject<string>;
  public currentUser: Observable<Account>;

  constructor(private httpClient: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Account>(
      JSON.parse(JSON.stringify(localStorage.getItem("id")))
    );
    this.currentUserToken = new BehaviorSubject<string>(
      localStorage.getItem("token")
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  // public get token(): string {
  //   return this.currentUserToken.value;
  // }
  _get_AccountDetail(): Observable<Account[]> {
    return this.httpClient.get<Account[]>(`${environment.BASE_URL}/account/get-account`)
  }
  _post_AccountDetailObj(accountObj: any): Observable<Account> {
    return this.httpClient.post<Account>(`${environment.BASE_URL}/account/account`, accountObj)
  }

  // getParentAccountName() {
  //   return this.httpClient.get(`${environment.BASE_URL}/users/get-parent`)
  // }
  // addParentAccount(parentObj: Account) {
  //   // const httpOptions = {
  //   //   headers: new HttpHeaders({
  //   //     "Content-Type": "application/json",
  //   //     Authorization: "Bearer " + localStorage.getItem('token')
  //   //   }),
  //   // };
  //   return this.httpClient.post(`${environment.BASE_URL}/users/account`, parentObj)
  // }
}
