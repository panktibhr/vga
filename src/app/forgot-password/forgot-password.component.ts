import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { LoginService } from "../@core/services/login.service";

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.css"],
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  submitted = false;
  error = false;
  showMsg: boolean = false;
  successMessage = "Email Sent successfully!";

  constructor(
    public formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit() {
    this.forgotPasswordForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
    });
  }
  get f() {
    return this.forgotPasswordForm.controls;
  }

  onSubmit() {
    this.successMessage = "Email Sent successfully!";
    this.error = false;
    this.showMsg = true;
    this.submitted = true;

    if (this.forgotPasswordForm.invalid) {
      return;
    }
    this.loginService.forgotPassword(this.f.email.value).subscribe(
      (data) => {
        if (data) {
          console.log(data);
          this.successMessage = "Email Sent Sccessfully!";
          // this.forgotPasswordForm.reset();
        } else {
          this.error = true;
        }
      },
      (error) => {
        this.error = error;
      }
    );
  }
}
