import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/@core/models/user.model';
import { LoginService } from 'src/app/@core/services/login.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  closeResult: string;
  userData: User;
  last_login: User;


  constructor(private modalService: NgbModal, private loginService: LoginService) { }

  ngOnInit() {
    // this.loginService.(this._up).subscribe((res: any) => {
    //   this.users = JSON.parse(JSON.stringify(res.data.recordset));
    // });
  }

  createData(editModal) {
  }
 
 

  update(id:User){
    // console.log(this.loginService.lastLogin(id));
    // this.loginService.lastLogin(id);
  }

  editUser(content) {
    this.modalService.open(content, { centered: true });
  }

}
