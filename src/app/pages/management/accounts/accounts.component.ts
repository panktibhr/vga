import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Account } from 'src/app/@core/models/account.model';
import { environment } from 'src/environments/environment';
import { AccountService } from '../../../@core/services/account.service';
import * as CryptoJS from 'crypto-js';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  closeResult: string;
  accountForm: FormGroup;
  parentAccountName: any;
  name: any = [];
  addParentObj: any;
  account_listing: any;
  account_listing_getData: any;
  selectedValue: string = '';
  _accountDetailObj$: Observable<Account[]>;
  _accountDetail$: Account[];

  constructor(public formBuilder: FormBuilder, private modalService: NgbModal, private accountservice: AccountService,
    private http: HttpClient) {
    this.accountForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      parent_account_id: ['', Validators.required]
    })
  }

  ngOnInit() {
    this._get_AccountsDetails();
  }
  onSubmit() {
    this.accountForm.markAllAsTouched();
    if (this.accountForm.valid) {
      this._post_AccountDetails();
      this.resetAccountForm();
    } else {
      console.log('Not valid');
    }
  }
  _get_ParentAccountName(id) {
    let match_ParentName = this._accountDetail$.filter((res: any) => {
      if (res.id == id) {
        return res.id;
      }
    })
    if (match_ParentName.length > 0) {
      return match_ParentName[0].name;
    } else {
      return 'NA';
    }
  }
  resetAccountForm() {
    this.accountForm.reset();
    this.modalService.dismissAll();
  }
  _get_AccountsDetails() {
    this._accountDetailObj$ = this.accountservice._get_AccountDetail();
    this._accountDetailObj$.subscribe((res: any) => {
      this._accountDetail$ = res.account;
    })
  }
  _post_AccountDetails() {
    this.accountservice._post_AccountDetailObj(this.accountForm.value).subscribe((res: any) => {
      if (res) {
        this._accountDetailObj$.subscribe((res: any) => {
          this._accountDetail$ = res.account;
        })
      }
    })
  }
  parent($event: any) {
    this.selectedValue = $event.target.value;
    console.log('Selected value = ' + this.selectedValue)
  }

  addAcoount(content) {
    this.modalService.open(content, { centered: true });
  }

}
