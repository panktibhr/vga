import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { NgxOrgChartModule } from 'ngx-org-chart';

import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from '../home/home.component';
import { GravatarModule } from 'ngx-gravatar';
import { SearchEmployeeComponent } from './Employee/search-employee/search-employee.component';
import { OrganizationalOverviewComponent } from './organizational-overview/organizational-overview.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UsersComponent } from './management/users/users.component';
import { AccountsComponent } from './management/accounts/accounts.component';
import { RolePermissionsComponent } from './masters/role-permissions/role-permissions.component';
import { ClientMasterComponent } from './Clients/client-master/client-master.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchClientComponent } from './Clients/search-client/search-client.component';
import { NewEmployeeComponent } from './Employee/new-employee/new-employee.component';
import { NewConsultantComponent } from './Consultants/new-consultant/new-consultant.component';
import { SearchConsultantsComponent } from './Consultants/search-consultants/search-consultants.component';
import { AccountService } from '../@core/services/account.service';

const routes: Routes = [
  //{ path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'organizational-overview', component: OrganizationalOverviewComponent },

  { path: 'users', component: UsersComponent },
  { path: 'accounts', component: AccountsComponent },
  { path: 'role-permissions', component: RolePermissionsComponent },
  { path: 'new-employee', component: NewEmployeeComponent },
  { path: 'search-employee', component: SearchEmployeeComponent },
  { path: 'NewClient', component: ClientMasterComponent },
  { path: 'SearchClient', component: SearchClientComponent },
  { path: 'NewConsultant', component: NewConsultantComponent },
  { path: 'SearchConsultants', component: SearchConsultantsComponent },
  { path: 'user-profile', component: UserProfileComponent }
];

@NgModule({
  declarations: [
    DashboardComponent,
    HomeComponent,
    SearchEmployeeComponent,
    OrganizationalOverviewComponent,
    UserProfileComponent,
    UsersComponent,
    AccountsComponent,
    RolePermissionsComponent,
    ClientMasterComponent,
    SearchClientComponent,
    NewEmployeeComponent,
    NewConsultantComponent,
    SearchConsultantsComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    GravatarModule,
    NgxOrgChartModule,
    RouterModule.forChild(routes)
  ],
  providers: [AccountService],
  exports: [
    DashboardComponent,
    HomeComponent
  ]
})
export class PagesModule { }
