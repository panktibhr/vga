import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-new-consultant',
  templateUrl: './new-consultant.component.html',
  styleUrls: ['./new-consultant.component.css']
})
export class NewConsultantComponent implements OnInit {

  consultantForm: FormGroup;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.consultantForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      consultantName: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required],
      phone: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      email: ['', Validators.required]
    })
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    if (this.consultantForm.valid) {
      console.log('form submitted');
    } else {
      this.validateAllFormFields(this.consultantForm);
    }
  }

}
