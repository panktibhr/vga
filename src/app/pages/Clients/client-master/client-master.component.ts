import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-client-master',
  templateUrl: './client-master.component.html',
  styleUrls: ['./client-master.component.css']
})
export class ClientMasterComponent implements OnInit {

  clientMasterForm: FormGroup;

  constructor(public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.clientMasterForm = this.formBuilder.group({
      clientName: ['', [Validators.required]],
      mobileNumber: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      companyName: ['', Validators.required],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', Validators.required],
      website: ['', Validators.required],
      contactPerson: ['', Validators.required],
      contactPersonDesignation: ['', Validators.required],
      contactPersonPhone: ['', Validators.required],
      contactPersonEmail: ['', Validators.required],
      chiefPerson: ['', Validators.required],
      chiefPersonDesignation: ['', Validators.required],
      chiefPersonPhone: ['', Validators.required],
      chiefPersonEmail: ['', Validators.required],
      industryType: ['', Validators.required],
      processType: ['', Validators.required],
      customerType: ['', Validators.required],
      businessScale: ['', Validators.required],
      ownershipType: ['', Validators.required],
      businessNature: ['', Validators.required],
      marketType: ['', Validators.required],
      intensity: ['', Validators.required],
      coreDrivingForce: ['', Validators.required],
      moderateDrivingForce: ['', Validators.required],
      minorDrivingForce: ['', Validators.required],
      coreCompetence: ['', Validators.required],
      moderateCompetence: ['', Validators.required],
      minorCompetence: ['', Validators.required],
      profitabilityStatus: ['', Validators.required]
    })
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    if (this.clientMasterForm.valid) {
      console.log('form submitted');
    } else {
      this.validateAllFormFields(this.clientMasterForm);
    }
  }

}
