import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-new-employee',
  templateUrl: './new-employee.component.html',
  styleUrls: ['./new-employee.component.css']
})
export class NewEmployeeComponent implements OnInit {

  employeeForm: FormGroup;

  constructor(public formBuilder: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {
    this.employeeForm = this.formBuilder.group({
      empName: ['', Validators.required],
      empTitle: ['', Validators.required],
      empLevel: ['', Validators.required],
      designation: ['', Validators.required],
      deptDiv: ['', Validators.required],
      reportingTo: ['', Validators.required],
      email: ['', Validators.required],
      mobileNumber: ['', [Validators.required, Validators.maxLength(13), Validators.minLength(10), Validators.pattern('^[+]*[0-9]+$')]],
      address: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      zipCode: ['', Validators.required]
    })
  }

  editDesignation(content) {
    this.modalService.open(content, { centered: false });
  }
  editDepartment(content) {
    this.modalService.open(content, { centered: false });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  onSubmit() {
    if (this.employeeForm.valid) {
      console.log('form submitted');
    } else {
      this.validateAllFormFields(this.employeeForm);
    }
  }

}
