import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationalOverviewComponent } from './organizational-overview.component';

describe('OrganizationalOverviewComponent', () => {
  let component: OrganizationalOverviewComponent;
  let fixture: ComponentFixture<OrganizationalOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganizationalOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationalOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
