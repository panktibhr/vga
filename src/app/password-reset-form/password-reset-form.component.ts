import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from "@angular/forms";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { Observable } from "rxjs";
import { first } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { LoginService } from "../@core/services/login.service";

@Component({
  selector: "app-password-reset-form",
  templateUrl: "./password-reset-form.component.html",
  styleUrls: ["./password-reset-form.component.css"],
})
export class PasswordResetFormComponent implements OnInit {
  changePasswordForm: FormGroup;
  submitted = false;
  returnUrl = "/signin";
  error = false;
  validationError = "";
  successMessage = "";
  resetId = "";
  showMsg: boolean = false;
  subscription: any;
  changePwd: Observable<any>;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    // this.activatedRoute.snapshot.queryParams["id"];
    console.log(this.activatedRoute.snapshot.queryParams["id"]);
    this.subscription = this.activatedRoute.snapshot.queryParams["id"];

    // this.subscription = this.activatedRoute.params.subscribe((params) => {
    //   this.resetId = params.id;
    //   console.log(this.resetId);
    // });
    this.changePasswordForm = this.formBuilder.group(
      {
        password: ["", [Validators.required]],
        confirmPassword: ["", [Validators.required]],
      },
      { validators: this.MatchPassword }
    );
  }

  MatchPassword(control: AbstractControl) {
    const password = control.get("password").value;
    const confirmPassword = control.get("confirmPassword").value;
    if (password !== confirmPassword) {
      control.get("confirmPassword").setErrors({ ConfirmPassword: true });
    } else {
      return null;
    }
  }

  get f() {
    return this.changePasswordForm.controls;
  }

  //   ngOnDestroy(): void {
  //     this.subscription.unsubscribe();
  //   }

  onSubmit() {
    console.log("button clicks...");
    this.error = false;
    this.submitted = true;
    this.successMessage = "Passward change Successfully!"
    this.showMsg = true;

    if (this.changePasswordForm.invalid) {
      this.changePasswordForm.markAllAsTouched();
      this.submitted = false;
      return false;
    }

    this.loginService
      .changePassword(
        this.subscription,
        this.f.password.value,
        this.f.confirmPassword.value
      )
      .subscribe((data: any) => {
        console.log(data);
        this.router.navigate([this.returnUrl]);
      });
    setTimeout(() => {
      window.location.href = this.returnUrl;
    }, 3000);

    // if (data) {
    //   console.log(data);
    //   this.router.navigate([this.returnUrl]);
    // }
  }
}
